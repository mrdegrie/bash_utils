#!/bin/bash

userdel -f "$1"

# shellcheck disable=SC2164
cd /usr/local/openvpn_as/scripts/

./sacli --user "$1" UserPropDelAll
echo 'OK'