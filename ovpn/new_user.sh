#!/bin/bash

useradd "$1"

# shellcheck disable=SC2086
yes $2 |passwd $1
echo "$1"
# shellcheck disable=SC2086
echo $2

# shellcheck disable=SC2164
cd /usr/local/openvpn_as/scripts/

./sacli --user "$1" --key "type" --value "user_connect" UserPropPut

./sacli --user "$1" --key "prop_autologin" --value "true" UserPropPut

./sacli --user "$1" --key "user_auth_type" --value "pam" UserPropPut